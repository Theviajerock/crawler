import requests
from bs4 import BeautifulSoup
import re
import json
import os

#Get the categories of the site
def getcategories(url):
    data = []
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    categories = soup.find_all(attrs={'class':'menuLink'})
    for categorie in categories[1:]:
        urlcat = "http://www.lacomer.com.mx/lacomer/"+categorie.get("href")
        data = data +getsubcategories(urlcat)
    return data

#Get the subcategories of the categorie
def getsubcategories(url):
    data = []
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    subcategories = soup.find_all(attrs={'class':'liga_a_suc'})
    print "Categorias aceptadas"
    if len(subcategories) == 0:
        print "esta llegando aca"
        categorie = soup.find(attrs={'class':'titulos_contenido'}).text
        subcategorie = categorie
        urlsub = url
        data = data + getelements(urlsub,categorie,subcategorie)
        return data
    categorie = soup.find(attrs={'class':'category-top'}).text.replace("\n", "").replace("  ","")
    print "Categorie grabada"
    for sub in subcategories:
        urlsub = "http://www.lacomer.com.mx/lacomer/"+sub.get("href")
        subcat = sub.text.replace("  ", "").replace("\n", "")
        elements = getelements(urlsub, categorie, subcat)
        data = data + elements
    return data

#def getpagination(url):
#    r = requests.get(url)
#    soup = BeautifulSoup(r.content, 'html.parser')
#    paginations = soup.find(attrs={'class':'pagination'})

#Get the elements of the subcategorie
def getelements(url, cat, subcategorie):
    lista = []
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    products = soup.find_all(attrs={'class':'product'})
    for product in products:
        urlproduct = product.find("a").get("href")
        title = product.find("h4").text.replace("  ","").replace("\n", "")
        price = product.find(attrs={'class':'price'}).find("a").text
        elements = getdata("http://www.lacomer.com.mx/lacomer/"+urlproduct)
        data = {"categorie":cat, "subcategorie":subcategorie,"title":title, "price":price,
        "content":elements[1],"code":elements[2], 'description':elements[3], 'imgurl':elements[0]}
        print data
        lista.append(data)
        print "BIEN"
    return lista

#Get the data of each element.
def getdata(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    image = soup.find(attrs={'class':'centerImg'}).get("src")
    product_code = soup.find(attrs={'class':'product-code'}).text
    cod_no_format = soup.find(attrs={'class':'product-code'}).text
    code_format=re.findall('[0-9]+', cod_no_format)[0]
    cont = soup.find(attrs={'class':'product-title'}).text.replace("  ", "").replace("\n","")
    if re.findall('\d\w+', cont) == []:
        code_content = "No info"
    else:
        code_content = re.findall('\d\w+', cont)[0].replace("  ", "").replace("\n", "")
    if re.findall('^\D+', cont) == []:
        description = "No info"
    else:
        description = re.findall('^\D+', cont)[0].replace("  ", "").replace("\n", "")
    data = [image, code_content,code_format, description]
    return data

url = "http://www.lacomer.com.mx/lacomer/doHome.action?key=Lomas-Anahuac&succId=14&succFmt=100"
data = getcategories(url)
data_to_json = json.dumps(data)

try:
    os.remove("data.json")
except OSError:
    pass

file_with_json = open("data.json","w")
file_with_json.write(str(data_to_json))
file_with_json.close()
